﻿Imports System.IO
Imports System.IO.Compression
Public Class Form1
    Dim ico As String

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            TextBox2.Text = FolderBrowserDialog1.SelectedPath
            My.Computer.FileSystem.CopyDirectory(TextBox2.Text, Path.GetTempPath + "\xapkexporterver1\Android", True)
        End If
    End Sub

    Private Sub FolderBrowserDialog1_HelpRequest(sender As Object, e As EventArgs) Handles FolderBrowserDialog1.HelpRequest

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            TextBox1.Text = OpenFileDialog1.FileName
            My.Computer.FileSystem.CopyFile(TextBox1.Text, Path.GetTempPath + "\xapkexporterver1\apk.apk", True)
        End If
    End Sub

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        My.Computer.FileSystem.CreateDirectory(Path.GetTempPath + "\xapkexporterver1")
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If OpenFileDialog2.ShowDialog() = DialogResult.OK Then
            ico = OpenFileDialog2.FileName
            My.Computer.FileSystem.CopyFile(ico, Path.GetTempPath + "\xapkexporterver1\icon.png")
            Dim img As Image = Image.FromFile(ico)
            PictureBox1.Image = img
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If TextBox1.Text = "" Then
            ShowMessage("Apk")
        Else
            If TextBox2.Text = "" Then
                ShowMessage("Android")
            Else
                Label7.Text = "Creating zip file"
                ZipFile.CreateFromDirectory(Path.GetTempPath + "xapkexporterver1", Path.GetTempPath + "\export.zip", CompressionLevel.Fastest, False)
                ProgressBar1.Increment(10)
                Label7.Text = "Converting to .apk"
                My.Computer.FileSystem.RenameFile(Path.GetTempPath + "\export.zip", "export.apk")
                ProgressBar1.Increment(10)
                If SaveFileDialog2.ShowDialog = DialogResult.OK Then
                    Label7.Text = "Converting to .xapk"
                    My.Computer.FileSystem.CopyFile(Path.GetTempPath + "\export.apk", SaveFileDialog2.FileName)
                    ProgressBar1.Increment(10)
                    Label7.Text = "Deleting temporary files"
                    My.Computer.FileSystem.DeleteFile(Path.GetTempPath + "\export.apk")
                    ProgressBar1.Increment(10)
                    System.IO.Directory.Delete(Path.GetTempPath + "\xapkexporterver1", True)
                    ProgressBar1.Increment(10)
                    System.Threading.Thread.Sleep(1000)
                    ProgressBar1.Value = 0
                    Label7.Text = "none"
                End If
            End If
        End If
    End Sub
    Private Function ShowMessage(errorcode As String)
        MessageBox.Show("You didn't included '" & errorcode & "'.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Function
End Class
